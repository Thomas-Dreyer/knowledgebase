module.exports = {
    title: 'React Knowhow Document',
    description: 'All I learned about React',
    base: '/help/',
    themeConfig: {
      sidebardepth: 3,
      sidebar: {
        '/' : ['','git','nodejs','react', 'reactNative','tools','mongodb'],
      }
    }
  }
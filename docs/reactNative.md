# React Native

## Projekt erstellen
Ein React Native Projekt sollte man mit Hilfe eines Programmes erzeugen, welches bereits die Sturktur anlegt. Das macht vieles einfacher. Das übliche ist 

```
yarn global add create-react-native-app
``` 

Damit wird im System ein Tool isntalliert, weches ein Projekt erzeugen kann. Ob es mit der Installation geklappt hat, sieht man mit 

```
create-react-native-app --help
``` 

Um nun ein Projekt anzulegen in den Folder gehen, unter dem das Projekt entstehen soll und dieses anlegen. Der Aufruf erzeugt das Projekt in einem darunter befindlichen Folder mit dem Projekt-Namen. 

```
create-react-native-app <ProjektName>
```

Soll das Projekt in einem GIT verwaltet werden, dann dieses anlegen und die Files hinzufügen. 

```
git init
git add .
git commit -m "Initial Setup"
```

## React Native Elements
Dieses Paket liefert Elemente wie z.B. ListItem, um damit React Native Anwendungen zu erstellen.
Details: [react-native-elements](https://react-native-elements.github.io/react-native-elements/)

## React Navigation
In diesem Paket befinden sich Navigationskomponenten, wie StackNavigator und weitere.
Details: [react-navigation](https://facebook.github.io/react-native/docs/navigation)
Beispiel: 
```js
import { View, Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation';

const MenuNavigator = createStackNavigator({
        Menu: { screen: Menu },
        Dishdetail: { screen: Dishdetail }
    },
    {
        initialRouteName: 'Menu',
        navigationOptions: {
            headerStyle: {
                backgroundColor: "#512DA8"
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: "#fff"            
            }
        }
    }
);

. . .

        <View style={{flex:1, paddingTop: Platform.OS === 'ios' ? 0 : Expo.Constants.statusBarHeight }}>
            <MenuNavigator />
        </View>
```
Die in der Navigation enthaltenen Komponenten sollten jeweils den Namen definieren, der dann nagezeigt wird, wenn der View aktiv ist. Das geschieht mit:
```js
    static navigationOptions = {
        title: 'Dish Details'
    };
```

## Redux installieren
Mit Redux wird ein Flux-Mechanismus in die App eingebracht. Interessante Bibliotheken sind:
* redux
* react-redux
* redux-thunk
* redux-logger
Zum Installieren in der App sind einige Schritte nötig. Zunächst müssen die ActionTypes definiert werden für die Events,
```js
//ActionTypes.js
export const DISHES_LOADING = 'DISHES_LOADING';

//reducer, z.B. dishes.js
import * as ActionTypes from './ActionTypes';
export const dishes = (state = { isLoading: true,
                                 errMess: null,
                                 dishes:[]}, action) => {
    switch (action.type) {
        case ActionTypes.DISHES_LOADING:
            return {...state, isLoading: true, errMess: null, dishes: []}
        default:
          return state;
      }
};
```
Der Store muss angelegt werden. Dabei werden hier combinedReducers benutzt, wodurch der State auf verschiedene Reducer aufgeteilt wird. Das ist in komplexeren Applikationen mit Sicherheit eine gute Idee:
```js
//configureStore.js
import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            dishes,
            comments,
            promotions,
            leaders
        }),
        applyMiddleware(thunk, logger)
    );

    return store;
}
```
Die ActionCreators dienen dazu die Aktonen aufzurufen. Hier im Beispiel mit dem Fetch sieht man einen thunk, erkennbar als Funktion, die eine Funktion liefert. Gleichzeitig wird die Fetch-Anweisung als Promise definiert:
```js
//ActionCreators.js - Reducer
import * as ActionTypes from './ActionTypes';
import { baseUrl } from '../shared/baseUrl';

export const fetchComments = () => (dispatch) => {
    return fetch(baseUrl + 'comments')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
    .then(response => response.json())
    .then(comments => dispatch(addComments(comments)))
    .catch(error => dispatch(commentsFailed(error.message)));
};

export const commentsFailed = (errmess) => ({
    type: ActionTypes.COMMENTS_FAILED,
    payload: errmess
});

export const addComments = (comments) => ({
    type: ActionTypes.ADD_COMMENTS,
    payload: comments
});
```
In der Main-App muss der Main-View durch einen Provider eingeschlossen werden. Dieser bekommt eine Instanz eines store übergeben. 
```js
//mainview
import {configureStore} from './configureStore';
const store = ConfigureStore();
export default class App extends Component {
    render() {
        return(
            <Provider store={store}>
                <Main />
            <Provider/>
        )
    }
}
```
Schließlich müssen noch alle Views mit einem Mapper für State auf Props versehen werden und die View beim Export mit connect an den Redux angebunden sein. 
```js
// componentXY.js
const mapStateToProps = state => {
    return {
      leaders: state.leaders
    }
  }

export default connect(mapStateToProps)(About);
```
Interessante Links:
* [Redux](https://redux.js.org/)
* [Redux on Github](https://github.com/reduxjs/redux)
* [React and Redux](https://redux.js.org/basics/usage-with-react/)
* [Redux Actions](https://redux.js.org/basics/actions/)
* [Redux Reducers](https://redux.js.org/basics/reducers/)
* [Redux Usage with React](https://redux.js.org/basics/usage-with-react/)
* [Redux Middleware](https://redux.js.org/advanced/middleware/)
* [Redux Thunk](https://github.com/reduxjs/redux-thunk)
* [Redux Logger](https://github.com/LogRocket/redux-logger)
* [The Flux Architecture](https://facebook.github.io/flux/)
* [Redux Tutorials](https://github.com/markerikson/react-redux-links/blob/master/redux-tutorials.md)
* [Flux Architecture In Depth Overview](https://facebook.github.io/flux/docs/in-depth-overview.html#content)

## Redux Store persistieren
Der Redux Store ist ohne weitere Module nur im RAM abgelegt. D.h. nach dem Neustarten der App ist der Inhalt wieder weg. Um den auch dauerhaft verfügbar zu machen, gibt es persistierungen. Eine sit im Paket 'redux-persist'. Dafür sind ein paar Anpassungen nötig:

```js
//configureStore.js
import {persistStore, persistCombineReducers} from 'redux-persist';
import storage from 'redux-persist/es/storage';

export const ConfigureStore = () => {

    //wird als erster Parameter in der createStore genutzt
    const config = {
        key: 'root',
        storage,
        debug: true
    };

    //persistCombineReducers nimmt nun als ersten Parameter das config auf
    const store = createStore(
        persistCombineReducers(config, {
            dishes,
            comments,
            promotions,
            leaders,
            favorites
        }),
        applyMiddleware(thunk, logger)
    );

    //erstellen des persistors (wird in App.js genutzt)
    const persistor = persistStore(store);

    //Rückgabe beider Klassen
    return {persistor, store};
}
```
In der App wird der Main nun zusätzlich zum Redux auch von der Persist-Komponente eingehüllt:
```js
//App.js

//diese Loading Komponente wird beim Rehydrate des Stores angezeigt (statt der Main)
import { Loading } from './components/LoadingComponent';

//store uns persistor holen
const {persistor, store} = ConfigureStore();

//Im provider wird nun noch der PersistGate eingebaut. Der nimmt zwei Params:
//  loading: Was soll beim Laden angezeigt werden
//  persistor: was soll persistiert/wiederhergestellt werden
export default class App extends React.Component {
  render() {
      return (
          <Provider store={store}>
            <PersistGate loading = {<Loading/>}
                         persistor={persistor}>
                <Main />
            </PersistGate>
          </Provider>
      );
  }
}

```
Das Verhalten ist dann identisch dem ohne dem Persistor. Nur dass der Inhalt des Stores über die Lebensdauer der App hinweg erhalten bleibt.

Interessante Links:
* [redux-persist](https://github.com/rt2zz/redux-persist)
* [The Definitive Guide to Redux Persist](https://blog.reactnativecoach.com/the-definitive-guide-to-redux-persist-84738167975)

## Debugging mit react-devtools
Diese Lib muss global installiert werden und bietet eine Applikation zum Ansehen der React-Native-Struktur sowie eine Möglichkeit die Events und Logs im Chrome-Browser zu sehen, die remote übertragen werden. 
Details: [react-devtools](https://facebook.github.io/react-native/docs/debugging)


## DatePicker Element 
Dieses Modul liefert einen hübschen Date-Time-Picker. 
Details: [react-native-datepicker](https://github.com/xgfe/react-native-datepicker)


## Listen erstellen
Listen werden mit einem Element FlatList und zum Design einer Zeile mit ListItem erstellt. Beispiel:

```js
function Menu(props) {

    const renderMenuItem = ({item, index}) => {

        return (
                <ListItem
                    key={index}
                    title={item.name}
                    subtitle={item.description}
                    hideChevron={true}
                    leftAvatar={{ source: require('./images/uthappizza.png')}}
                  />
        );
    };

    return (
            <FlatList 
                data={props.dishes}
                renderItem={renderMenuItem}
                keyExtractor={item => item.id.toString()}
                />
    );
}
```

## Cards erstellen
Cards werden mit dem Card-Element aus react-native-elements erstellt. Beispiel
```js
    <Card
    featuredTitle={dish.name}
    image={require('./images/uthappizza.png')}>
        <Text style={{margin: 10}}>
            {dish.description}
        </Text>
    </Card>
```

## Stack-Navigation erstellen
Damit werden Seiten gestapelt. Automatisch wird beim Aufruf einer Unterseite dann ein Zurück-Button oben links erzeugt.

```js
const HomeNavigator = createStackNavigator({
    Home: { screen: Home }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff"  
    })
});
```

## DrawerNavigator erstellen
Dies erzeugt ein von links einblendendes Menu zum navigeren. Beispiel:
```js
const MainNavigator = createDrawerNavigator({
    Home: 
      { screen: HomeNavigator,
        navigationOptions: {
          title: 'Home',
          drawerLabel: 'Home'
        }
      },
    Menu: 
      { screen: MenuNavigator,
        navigationOptions: {
          title: 'Menu',
          drawerLabel: 'Menu'
        }, 
      }
}, {
  drawerBackgroundColor: '#D1C4E9'
});
...
//später im View
    <View>
        <MainNavigator />
    </View>
```

## ScrollView
Mit diesem Element wird der Inhalt scrollbar gemacht:
```js
<ScrollView>
    <RenderItem item={this.state.dishes.filter((dish) => dish.featured)[0]} />
    <RenderItem item={this.state.promotions.filter((promo) => promo.featured)[0]} />
    <RenderItem item={this.state.leaders.filter((leader) => leader.featured)[0]} />
</ScrollView>
```

## Buttons/Icons erstellen
Mit einem Icon kann sowohl ein Icon als auch ein Button erstellt werden:
```js
<Icon
    raised
    reverse
    name={ props.favorite ? 'heart' : 'heart-o'}
    type='font-awesome'
    color='#f50'
    onPress={() => props.favorite ? console.log('Already favorite') : props.onPress()}
    />
```

## Verhindern, dass ein View in Bereiche malt, die nicht sichtbar sin
Insbesondere bei neuen Smartphones hat der Bildschirm am oberen Rand eine Notch (z.B. iPhone X). Damit ein View diese nicht mit Inhalt füllt, nimmt man einen SafeAreaView:

```js
    <ScrollView>
      <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
            ...
      </SafeAreaView>
    </ScrollView>
```
## Forms
React Native unterstützt Forms und Modals. Diese sind dann zwar nicht das gleiche wie im HTML, aber es wird so in etwa nachempfunden. So können Nutzerdaten abgefragt werden. React bietet dafür die entsprechenden Controls an, die einfach in eine normale Komponente eingebaut werden:

```js

    handleReservation() {
       //was immer dann geschehen soll
    }
...
    render() {
        return(
            <ScrollView>
                <View style={styles.formRow}>
                    <Text style={styles.formLabel}>Number of Guests</Text>
                    <Picker
                        style={styles.formItem}
                        selectedValue={this.state.guests}
                        onValueChange={(itemValue, itemIndex) => this.setState({guests: itemValue})}>
                        <Picker.Item label="1" value="1" />
                        <Picker.Item label="2" value="2" />
                        <Picker.Item label="3" value="3" />
                        <Picker.Item label="4" value="4" />
                        <Picker.Item label="5" value="5" />
                        <Picker.Item label="6" value="6" />
                    </Picker>
                </View>
                ....
                <View style={styles.formRow}>
                    <Button
                        onPress={() => this.handleReservation()}
                        title="Reserve"
                        color="#512DA8"
                        accessibilityLabel="Learn more about this purple button"
                        />
                </View>
            </ScrollView>
        );
    }
```

## Modals
Damit werden modale Dialoge nachgebildet. Die Sichtbarkeit wird über den State der Komponente gesteuert:
```js
import { Text, View, StyleSheet, Picker, Switch, Button, Modal } from 'react-native';
...

        this.state = {
            guests: 1,
            smoking: false,
            date: '',
            showModal: false
        }   
...
    toggleModal() {
        this.setState({showModal: !this.state.showModal});
    }

    handleReservation() {
        this.toggleModal();
    }

    resetForm() {
        this.setState({
            guests: 1,
            smoking: false,
            date: '',
            showModal: false
        });
    }
...
    <Modal  animationType = {"slide"} 
            transparent = {false}
            visible = {this.state.showModal}
            onDismiss = {() => this.toggleModal() }
            onRequestClose = {() => this.toggleModal() }>
        <View style = {styles.modal}>
            <Text style = {styles.modalTitle}>Your Reservation</Text>
            <Text style = {styles.modalText}>Number of Guests: {this.state.guests}</Text>
            <Text style = {styles.modalText}>Smoking?: {this.state.smoking ? 'Yes' : 'No'}</Text>
            <Text style = {styles.modalText}>Date and Time: {this.state.date}</Text>
            <Button 
                onPress = {() =>{this.toggleModal(); this.resetForm();}}
                color="#512DA8"
                title="Close" 
                />
        </View>
    </Modal>
```
Das erzeut einen reinfahrenden Screen, der mit showModal=true erscheint und bei showModal=false wieder verschwindet. Mit dem Button kann man dann ggf. eine Aktion verbinden. Hier schließt der nur den Modal.

Details:
* [RNE Input](https://react-native-elements.github.io/react-native-elements/docs/input.html)
* [Modal](https://facebook.github.io/react-native/docs/modal.html)

## EMails senden
EMails können einfach mit dem Modul 'expo-mail-composer' versendet werden. Dazu in der App den MailComposer einbinden und konfigurieren.

```js
import * as MailComposer* from 'expo-mail-composer';

...
// Senden in der Funktion mit dem MailComposer
sendMail() {
        MailComposer.composeAsync({
        recipients: ['confusion@food.net'],
        subject: 'Enquiry',
        body: 'To whom it may concern:'
    })
}
...
    //versenden per Button im JSX
    <Button
        title="Send Email"
        buttonStyle={{backgroundColor: "#512DA8"}}
        icon={<Icon name='envelope-o' type='font-awesome' color='white' />}
        onPress={this.sendMail}
        />

```
Interessante Links:
* [Mail Composer](https://docs.expo.io/versions/latest/sdk/mail-composer/)

## Social Sharing
Sharen mit einer belibeigen App des Betriebssystems geschiet mit der Share Componente aus ReactNative. 

```js
import { Share } from 'react-native';

//Funktion zum sharing
const shareDish = (title, message, url) => {
    Share.share({
        title: title,
        message: title + ': ' + message + ' ' + url,
        url: url
    },{
        dialogTitle: 'Share ' + title
    })
}
...
//Im JSX die Funktion rufen
<Icon
    raised
    reverse
    name='share'
    type='font-awesome'
    color='#51D2A8'
    style={styles.cardItem}
    onPress={() => shareDish(dish.name, dish.description, baseUrl + dish.image)} />
```
Interessante Links
* [Share API](https://reactnative.dev/docs/share.html)

## Bilder von der Kamera aufnehmen
Das kann mit dem Paket 'expo-image-picker' gemacht werden. Zunächst müssen sich die Rechte geholt werden, dann kann die Kamera genutzt werden:

```js
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';

getImageFromCamera = async () => {
    const cameraPermission = await Permissions.askAsync(Permissions.CAMERA);
    const cameraRollPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    if (cameraPermission.status === 'granted' && cameraRollPermission.status === 'granted') {
        let capturedImage = await ImagePicker.launchCameraAsync({
            allowsEditing: true,
            aspect: [4,3]
        });

        if (!capturedImage.cancelled) {
            this.processImage(capturedImage.uri);
        }
    }
}
```

Interessante Links:
* [ImagePicker](https://docs.expo.io/versions/v27.0.0/sdk/imagepicker)

## Bilder verändern
Bilder ändern kann mit dem Paket 'expo-image-manipulator' geschehen:

```js
import * as ImageManipulator from 'expo-image-manipulator';

    processImage= async (imageUri) => {
        let processedImage = await ImageManipulator.manipulateAsync(
            imageUri, 
            [
                {resize: {width:400}}
            ],
            {format: 'png'}
        );
        this.setState({imageUrl: processedImage.uri});
    }
```

Das Beispiel reduziert die Größe des Bildes auf 400 Pixel horizontal und behält das Aspect-Ratio bei. Zusätzlich wird das Bild in ein png gewandelt und dann gespeichert.

Interessante Links:
* [ImageManipulator](https://docs.expo.io/versions/v27.0.0/sdk/imagemanipulator)
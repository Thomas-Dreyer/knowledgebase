# Node.js und NPM

## Was ist Node.js und NPM?
Node ist eine JavaScript Runtime, die außerhalb vom Browser ausgeführt werden kann. Dazu gibt es tausende von Paketen, um alles mögliche zu tun. Node ermöglicht das Ausführen von JavaScript auf einem Rechner.

![Node.js Aufbau](./images/NodeJSSchema.png)

NPM ist der Node-Package-Manager. Mit dem werden Node-Js Pakete verwaltet.

## Initialize package.json file
Sinn des Files:
* Dokumentation der verwendeten Versionen
* Definition welche Version für das Projekt benötigt wird
* Macht den build reproduzierbar
Erzeugen mit:
```
npm init
```
Dann die Fragen beantworten. Im Ergebnis entsteht ein package.json file.

## Pakete installiern
Um ein lokales Paket zu installieren in das Verzeichnis wechseln und dann ausführen:
```
npm install --save-dev <paket>
```
einige Pakete sind nur als globale pakete verfügbar. Diese werden dann für alle Projekte gleichermaßen verwendet. Dann mit -g option installieren:
```
npm install -g <paket>
```

## Node Module schreiben
Jede JavaScript-Datei in Node wird als Modul bezeichnet. Ein neues Modul im Sinne einer Applikation wird in einem neuen Pfad erzeugt. In diesem wird mit dem Ausführen von 
``` 
npm init
```
Die Erstellung eines neuen Moduls angestosen. Auf der Kommandozeile werden Fragen gestellt, die zum Ausfüllen des files package.json dienen. Ein fertiges Beispiel sieht dann so aus
```json
{
  "name": "node-examples",
  "version": "1.0.0",
  "description": "Simple Node Examples",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "node index"
  },
  "author": "Jogesh Muppala",
  "license": "ISC"
}
```
Der Code wird dann in einem JavaScript-File erzeugt, miest im index.js. Ausgeführt wird das File mit npm start bzw. dem Script, welches im package.jsn definiert wurde.

## HTTP-Server in Node
Das HTTP-Module ist Teil des Node-Lieferumfanges. Damit kann ein einfacher Server erstellt werden. Ein beispiel ist hier ersichtlich:
```js
const http = require('http');

const hostname = 'localhost';
const port = 3000;

const server = http.createServer((req, res) => {
    console.log(req.headers);
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.end('<html><body><h1>Hello, World!</h1></body></html>');
})

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```

## Express Framework für Server
Das Framework Express erleichtert das Schreiben eigener Server. Es muss aus dem Paket express installiert werden. Ein simpler Server sieht dann so aus:
```js
const express = require('express'),
     http = require('http');

const hostname = 'localhost';
const port = 3000;

const app = express();

app.use((req, res, next) => {
  console.log(req.headers);
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.end('<html><body><h1>This is an Express Server</h1></body></html>');

});

const server = http.createServer(app);

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```

## Express-Module Morgan
Mit dem Modul Morgan kann im Express ein einfaches Logging realisiert werden. Das Modul wird gleich am Anfange in das Express eingehangen:
```js
const morgan = require('morgan');

...

app.use(morgan('dev'));
```
Damit wird jeder Request an den Server mitgeloggt. 

![Morgan Logger](./images/Morgan-Logger.png)

## Express-Router
Mit diesem Modul kann recht einfach eine REST-API erstellt werden. Als best practice hat sich dabei ein Unterverzeichnis routes gezeigt. In diesem wird jeweils die Route abgelegt, die dann in der app.js verlinkt wird. Hier ein Beispiel, welches eine Implementierung der get, post, put und delete HTTP-Verben ermöglicht.
```js
const express = require('express');
const bodyParser = require('body-parser');

const dishRouter = express.Router();

dishRouter.use(bodyParser.json());

dishRouter.route('/')
.all((req,res,next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req,res,next) => {
    res.end('Will send all the dishes to you!');
})
.post((req, res, next) => {
    res.end('Will add the dish: ' + req.body.name + ' with details: ' + req.body.description);
})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /dishes');
})
.delete((req, res, next) => {
    res.end('Deleting all dishes');
});

module.exports = dishRouter;
```

In der Applikation wird da dann so eingebracht, in dem die URL-Route definiert wird:
```js
const dishRouter = require('./routes/dishRouter');
app.use('/dishes', dishRouter);
```

Interessante Links:
* [Express](http://expressjs.com/)

## Express-Generator
Dieses Tool dient dazu eine Express-Applikation schnell zu erstellen udn nicht alles manuell anlegen zu müssen. Dafür muss diese aber auch global installiert werden. Dann kann das Tool express aufgerufen werden, um den Applikationsrahmen zu erstellen.
```
npm install -g express-generator
express --view=pug myapp
```
Im Ergebnis entsteht dann ein kompletter Verzeichnisbaum für die Anwendung mit den Dateien, die dafür benötigt werden. Nach der Erstellung in das Verzeichnis wechseln und mit npm install die definierten Pakete noch installieren.
```
cd myapp
npm install
```
Die fertige Struktur sieht dann so aus:

![Ausgabe des Express Generator](./images/Express-Generator.png)

Interessante Links:
* [Express Generator](http://expressjs.com/en/starter/generator.html)

## Basic Authentication
Damit wird es möglich einen Benutzer in die Aufrufe des HTTP-Servers zu übermitteln und so Rechtemanagement zu realisieren.

Interessante Links:
* [Basic Authentication](https://en.wikipedia.org/wiki/Basic_access_authentication)
* [Mozilla Basic Authentication](https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication)

Eine einfache Umsetzung kann mit Bordmitteln von Express geschehen. Dazu wird eine Funktion geschaffen:
```js
function auth (req, res, next) {
  console.log(req.headers);
  var authHeader = req.headers.authorization;
  if (!authHeader) {
      var err = new Error('You are not authenticated!');
      res.setHeader('WWW-Authenticate', 'Basic');
      err.status = 401;
      next(err);
      return;
  }

  var auth = new Buffer.from(authHeader.split(' ')[1], 'base64').toString().split(':');
  var user = auth[0];
  var pass = auth[1];
  if (user == 'admin' && pass == 'password') {
      next(); // authorized
  } else {
      var err = new Error('You are not authenticated!');
      res.setHeader('WWW-Authenticate', 'Basic');      
      err.status = 401;
      next(err);
  }
}
```
Diese wird dann als Layer in das Express eingebracht:
```js
app.use(auth);
```
Damit wird jeder Ruf geprüft, ob ein Nutzername und Passwort enthalten ist und nur wenn das passt auch bearbeitet.

## Nutzen von Cookies
Cookies werden auf Client-Seite gespeichert und bei jedem Aufruf wieder mit an den Server übermittelt. Der Server kann diese dann erzeugen und dem Client zurückgeben:
```js
app.use(cookieParser('12345-67890-09876-54321'));

function auth (req, res, next) {

  if (!req.signedCookies.user) {
    var authHeader = req.headers.authorization;
    if (!authHeader) {
        var err = new Error('You are not authenticated!');
        res.setHeader('WWW-Authenticate', 'Basic');              
        err.status = 401;
        next(err);
        return;
    }
    var auth = new Buffer.from(authHeader.split(' ')[1], 'base64').toString().split(':');
    var user = auth[0];
    var pass = auth[1];
    if (user == 'admin' && pass == 'password') {
        res.cookie('user','admin',{signed: true});
        next(); // authorized
    } else {
        var err = new Error('You are not authenticated!');
        res.setHeader('WWW-Authenticate', 'Basic');              
        err.status = 401;
        next(err);
    }
  }
  else {
      if (req.signedCookies.user === 'admin') {
          next();
      }
      else {
          var err = new Error('You are not authenticated!');
          err.status = 401;
          next(err);
      }
  }
}
```

Interessante Links:
* [cookie-parser](https://github.com/expressjs/cookie-parser)
* [express-session](https://github.com/expressjs/session)
* [session-file-store](https://www.npmjs.com/package/session-file-store)
* [HTTP Cookies](https://en.wikipedia.org/wiki/HTTP_cookie)
* [Sessions in Express.js](http://expressjs-book.com/index.html%3Fp=128.html)
* [Express Session Management](https://javabeat.net/expressjs-session-management/)

## Express Sessions
Sessions sind am Server gespeicherte Kontexte eines Clients. Der Client erhält ein Session-Cookie mit einer ID und übermittelt diese dann immer mit an den Server. Anhand dieser ID kann der Server dann auf die Session-Informationen zugreifen. Über den File-Store wird jede Session in eine Datei gespeichert.

```js
. . .

var session = require('express-session');
var FileStore = require('session-file-store')(session);

. . .

app.use(session({
  name: 'session-id',
  secret: '12345-67890-09876-54321',
  saveUninitialized: false,
  resave: false,
  store: new FileStore()
}));

function auth (req, res, next) {
    console.log(req.session);

    if (!req.session.user) {
        var authHeader = req.headers.authorization;
        if (!authHeader) {
            var err = new Error('You are not authenticated!');
            res.setHeader('WWW-Authenticate', 'Basic');                        
            err.status = 401;
            next(err);
            return;
        }
        var auth = new Buffer.from(authHeader.split(' ')[1], 'base64').toString().split(':');
        var user = auth[0];
        var pass = auth[1];
        if (user == 'admin' && pass == 'password') {
            req.session.user = 'admin';
            next(); // authorized
        } else {
            var err = new Error('You are not authenticated!');
            res.setHeader('WWW-Authenticate', 'Basic');
            err.status = 401;
            next(err);
        }
    }
    else {
        if (req.session.user === 'admin') {
            console.log('req.session: ',req.session);
            next();
        }
        else {
            var err = new Error('You are not authenticated!');
            err.status = 401;
            next(err);
        }
    }
}
```
Interessante Links:
* [cookie-parser](https://github.com/expressjs/cookie-parser)
* [express-session](https://github.com/expressjs/session)
* [session-file-store](https://www.npmjs.com/package/session-file-store)
* [HTTP Cookies](https://en.wikipedia.org/wiki/HTTP_cookie)
* [Sessions in Express.js](http://expressjs-book.com/index.html%3Fp=128.html)
* [Express Session Management](https://javabeat.net/expressjs-session-management/)

## HTTPS einrichten
Heute sollten ja alle Server mit HTTPS arbeiten, nicht mit dem gewöhnlichen HTTP. Daher muss der Server mit dem Secure Protocols aufgerüstet werden.
Dazu brauchen wir erst mal ein Zertifikat. Da wir grad nicht viel ausgeben wollen, nehmen wir mal ein selbstsigniertes. D kann man sich mit OpenSSL erstellen. Dazu die Software installieren oder auf dem MAC:

```
openssl genrsa 1024 > private.key
openssl req -new -key private.key -out cert.csr
openssl x509 -req -in cert.csr -signkey private.key -out certificate.pem
```

Damit haben nun drei Dateien: 
* private.key: Den privaten Schlüssel. Den gut aufbewahren, kein anderer sollte den sehen
* cert.csr: brauchen wir erst mal nicht
* certificate.pem: Das Zertifikate, welches dann dem Client gesendet wird

Zum Einstellen der HTTPS-Verbindung im Folder /bin die Datei www editieren:
```js
. . .
var https = require('https');
var fs = require('fs');
. . .
app.set('secPort',port+443);
. . .

/**
 * Create HTTPS server.
 */ 
 
var options = {
  key: fs.readFileSync(__dirname+'/private.key'),
  cert: fs.readFileSync(__dirname+'/certificate.pem')
};

var secureServer = https.createServer(options,app);

/**
 * Listen on provided port, on all network interfaces.
 */

secureServer.listen(app.get('secPort'), () => {
   console.log('Server listening on port ',app.get('secPort'));
});
secureServer.on('error', onError);
secureServer.on('listening', onListening);
. . .
```
anschlißend noch in der App.js:
```js
. . .
// Secure traffic only
app.all('*', (req, res, next) => {
  if (req.secure) {
    return next();
  }
  else {
    res.redirect(307, 'https://' + req.hostname + ':' + app.get('secPort') + req.url);
  }
});
. . .
```
Und damit sollte es dann laufen.


## Upload von Dateien
Um einen Upload von Files zu gewährleisten setzen wir auf die form/data mit MultiPart. Als Modul eigenet sich dafür multer. Also installieren und dann einen REST endpoint anlegen:

```js
const express = require('express');
const bodyParser = require('body-parser');
const authenticate = require('../authenticate');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/images');
    },

    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
});

const imageFileFilter = (req, file, cb) => {
    if(!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return cb(new Error('You can upload only image files!'), false);
    }
    cb(null, true);
};

const upload = multer({ storage: storage, fileFilter: imageFileFilter});

const uploadRouter = express.Router();

uploadRouter.use(bodyParser.json());

uploadRouter.route('/')
.get(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    res.statusCode = 403;
    res.end('GET operation not supported on /imageUpload');
})
.post(authenticate.verifyUser, authenticate.verifyAdmin, upload.single('imageFile'), (req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(req.file);
})
.put(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /imageUpload');
})
.delete(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    res.statusCode = 403;
    res.end('DELETE operation not supported on /imageUpload');
});

module.exports = uploadRouter;
```
Damit haben wir einen Endpoint, der ein Post akzeptiert und das File abspeichert, sofern es ein Bild ist.
In der App.js muss noch der Endpunkt konfiguriert werden:
```js
. . .
const uploadRouter = require('./routes/uploadRouter');
. . .
app.use('/imageUpload',uploadRouter);
. . .
```
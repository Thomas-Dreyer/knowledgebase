# Mongo DB

## Installation der Mongo DB
Zur Installation am Besten auf die Seite  [http://www.mongodb.org](http://www.mongodb.org) gehen. Dort ist das jeweils beschrieben, wie die Installation läuft. Im Folgenden gehen wir davon aus, dass das geklappt hat.

Dann einen Pfad irgendwo anlegen, in dem die Datenbank liegen soll. Wir nehmen nun an, dass die Daten in einem Unterpfad data liegen sollen. Aufruf des Dienstes dann mit:
```
mongod --dbpath=data --bind_ip 127.0.0.1
```
Damit wird eine leere Datenbank gestartet und lokal verfügbar gemacht.

Um mit der Datenbank arbeitne zu können, kann die Kommandozeile genutzt werden. Das Tool dazu heißt mongo. Da drinnen gibt es eine JavaScript-ähnliche Syntax zum Aufrufen von Funktionen:

```
// Aufruf
mongo

// einfache Befehle, Anlegen einer Datenbank
> db
> use conFusion
> db.help()

//Daten einfügen
db.dishes.insert({ name: "Uthappizza", description: "Test" });

//Daten suchen
db.dishes.find().pretty();
```

## Node-Module MongoDB
Zum Einfachen Zugriff auf die Mongo-DB gibt es ein Node-Modul. Das einfach installieren und dann kann im Node auf die Datenbank zugegriffen werden.
```js
//pakete nutzen
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

//Zugang zur DB
const url = 'mongodb://localhost:27017/';
const dbname = 'conFusion';

//Verbinden
MongoClient.connect(url, (err, client) => {

    assert.equal(err,null);

    console.log('Connected correctly to server');

    //Datenbank öffnen
    const db = client.db(dbname);
    const collection = db.collection("dishes");

    //Datensatz einfügen
    collection.insertOne({"name": "Uthappizza", "description": "test"},
    (err, result) => {
        assert.equal(err,null);

        console.log("After Insert:\n");
        console.log(result.ops);

        //Datensatz auslesen
        collection.find({}).toArray((err, docs) => {
            assert.equal(err,null);
            
            console.log("Found:\n");
            console.log(docs);

            //Collection wieder löschen
            db.dropCollection("dishes", (err, result) => {
                assert.equal(err,null);

                client.close();
            });
        });
    });
});
```

Interessante Links
* [MongoDB Native Driver](https://github.com/mongodb/node-mongodb-native)
* [MongoDB NodeJS Native Driver Documentation](http://mongodb.github.io/node-mongodb-native/)
* [Callback Hell](http://callbackhell.com/)
* [Bluebird](http://bluebirdjs.com/docs/getting-started.html)
* [Managing Node.js Callback Hell with Promises, Generators and Other Approaches](https://strongloop.com/strongblog/node-js-callback-hell-promises-generators/)
* [The great escape from Callback Hell](https://medium.com/@js_tut/the-great-escape-from-callback-hell-3006fa2c82e)

## Mongoose
Die Mongo-DB speichert unstrukturierte Dokumente als JSON Files ab. Das ist manchmal nicht so hilfreich, wenn man bestimmte Strukturen erwartet. Das Framework Mongoose erlaubt es nun Strukturen zu definieren. Daszu das Paket mongoose installieren. Dann ein Schema definieren:

```js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Define a schema with a field name and description and add timestamps
const dishSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true
    }
},{
    timestamps: true
});

//create the schema an bind it to collection
var Dishes = mongoose.model('Dish', dishSchema);

//make the schema available
module.exports = Dishes;
```
Damit kann dann gearbeitet werden:
```js
const mongoose = require('mongoose');

//import Schema 
const Dishes = require('./models/dishes');

//connection to MongoDB
const url = 'mongodb://localhost:27017/conFusion';
const connect = mongoose.connect(url);

//connect to db
connect.then((db) => {

    console.log('Connected correctly to server');

    //create a new document
    var newDish = Dishes({
        name: 'Uthappizza',
        description: 'test'
    });

    //and save it to the database
    newDish.save()
        .then((dish) => {
            console.log(dish);
            // look for it 
            return Dishes.find({});
        })
        .then((dishes) => {
            console.log(dishes);
            // delete it
            return Dishes.remove({});
        })
        .then(() => {
            //close connection
            return mongoose.connection.close();
        })
        .catch((err) => {
            console.log(err);
        });
});
```

Schemas können auch ineinander gestapelt werden. Hier ein Beispiel für Kommentare in den Dishes. Es wird ein Array von Kommentaren unter comments verfügbar gemacht.
```js
var commentSchema = new Schema({
    rating:  {
        type: Number,
        min: 1,
        max: 5,
        required: true
    },
    comment:  {
        type: String,
        required: true
    },
    author:  {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

var dishSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true
    },
    comments:[commentSchema]
}, {
    timestamps: true
});
```

Interessante Links:
* [Mongoose](https://mongoosejs.com/)
* [Mongoose Documentation](https://mongoosejs.com/docs/guide.html)
* [Mongoose Currency](https://www.npmjs.com/package/mongoose-currency)

# React
## Historisches
Einige Fakten zu React:
* Designed by Jordan Walke
* First deployed on Facebook's newsfeed in 2011
* Open-sourced at JSConf US in May 2013
* Designed for speed, simplicity, and scalability

[React WIKI Referenz](https://en.wikipedia.org/wiki/React_(JavaScript_library))

## Getting started
Um ein neues React-Projekt anzulegen, kann das NPM-Paket create-react-app genutzt werden. Dieses dann einfach ausführen und die Fragen beantworten.
````
create-react-app
````

## JSX
JSX ist eine Syntax-Erweiterung zum JavaScript. Damit kann ein HTML-ähnlicher Code erzeugt werden, der als Basis im React dient. Beispiel:
```
const dish = { id: 0, name:'Uthappizza’, . . .};

return (
    <div key={dish.id} className="col-12 mt-5">
        <Media tag="li">
            <Media body className="ml-5">
                <Media heading>{dish.name} </Media> 
            </Media>
        </Media> 
    </div>
);
```

## Components
Im React werden Komponenten erzeugt, die dann zu einer Anwendung zusammengefügt werden. Eine Komponente ist ein Satz von React-Elementen, die auf dem Bildschirm erscheinen sollen. Damit kann die UI in kleine Teile zerlegt werdne, die sich einfacher pflegen lassen.

### Konventionen
* User-defined Komponenten starten immer mit einem Großbuchstaben 
* Tags mit Kleinbuchstaben werden als DOM tags interpretiert (z.B. div)

### State 
Jede Komponente kann ihren Zustand im State speichern. Dieser ist private und unter voller Kontrolle der Komponente. Nur Klassen-Komponenten haben einen Zustand.
Beispiel:

```JavaScript
class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDish: null
        }
    }
}
```

Der Zustand darf niemals direkt, sondern nur über die Funktion setState() gesetzt werden!

```JavaScript
//correct
onDishSelect(dish) {
    this.setState({
        selectedDish: dish
    }
}

// never !
this.state.selectedDish = dish;
```


### Props
Jede Komponente hat Properties (Props). Diese werden an die Komponente mit Parametern im JSX übergeben. Innerhalb der Komponente sind die dann als props erreichtbar. Es kann auch mehrere Parameter geben. Die Komponente kann die props nicht ändern. Beispiel:

    <Menu dishes={this.state.dishes} />

## Events
Handling ist vergleichbar mit Events im DOM. Events sollten in camelCase benannt werden. 

    <Card onClick={() => this.onDishSelect(dish)}>

### LifeCycle Events
Während der Abarbeitung der React-Applikation gibt es diverse Einsprungpunkte in den LifeCycle. Wichtige sind:
* constructor()
* getDerivedStateFromProps()
* shouldComponentUpdate()
* render()
* getSnapshotBeforeUpdate()
* componentDidUpdate()
* componentDidMount()
Durch Überschreiben der Funktion kann eigener Code eingebracht werden. 

## React Router
Für diese Funktion ist das NPM-Modul react-router-dom notwendig.
Dann wird diese als BrowserRouter eingebracht:

```JavaScript
import React, {Component} from 'react';
import Main from './components/MainComponent';
import './App.css';
import {BrowserRouter} from 'react-router-dom';
 
 
 class App extends Component {
   render() {
     return (
      <BrowserRouter>
        <div> 
          <Main />
        </div>
      </BrowserRouter>
     );
   };
 }
```

Um zu navigieren kann dann z.B im Menu die Klasse Link genutzt werden:

```JavaScript
import {Link} from 'react-router-dom';
```
```html
<div className="row justify-content-center">             
    <div className="col-4 offset-1 col-sm-2">
        <h5>Links</h5>
    <ul className="list-unstyled">
        <li><Link to="/home">Home</Link></li>
        <li><Link to="/aboutus">About</Link></li>
        <li><Link to="/menu">Menu</Link></li>
        <li><Link to="/contactus">Contact</Link></li>
    </ul>
</div>
```

Um das in das Menu einzubauen wird die Klasse NavLink genutzt:
```JavaScript
import {NavLink} from 'react-router-dom';
```
```html
<div>
    <Navbar className="navbar-dark navbar-expand-md" >
        <div className="container">
            <NavbarToggler onClick={this.toggleNav} />
            <NavbarBrand className="mr-auto" href="/"><img src='assets/images/logo.png' height="30" width="41" alt='Ristorante Con Fusion' /></NavbarBrand>
            <Collapse isOpen={this.state.isNavOpen} navbar>
                <Nav navbar>
                <NavItem>
                    <NavLink className="nav-link"  to='/home'><span className="fa fa-home fa-lg"></span> Home</NavLink>
                </NavItem>
            </Collapse>
        </div>
    </Navbar>
</div>
```

Dazu in der Main-Komponente einen Switch einbauen. Je nach ausgewählter Route wird dann eine andere Komponente eingeblendet. 

```html
<Switch>
    <Route path="/home" component={HomePage} />
    <Route exact path="/contactus" component={Contact} />
    <Route exact path="/menu" component={() => <Menu dishes={this.state.dishes} /> } />
    <Redirect to="/home" />
</Switch>
```

Um auf ein spezielles Element zu routen kann dies in der Definition des Switch vorgesehen werden:
 
```html
<Switch>
    <Route path="/home" component={HomePage} />
    <Route exact path="/contactus" component={Contact} />
    <Route exact path="/menu" component={() => <Menu dishes={this.state.dishes} /> } />
    <Route path="/menu/:dishId" component={DishWithId} />
    <Redirect to="/home" />
</Switch>
```

## Controlled Forms
Das sind Forms, deren Daten in der Komponente behandelt werden. Die Daten werden dabei im State gespeichert. Somit muss zunächst der State definiert sein:

```JavaScript
class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            telnum: '',
            email: '',
            agree: false,
            contactType: 'Tel.', 
            message: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }
```
In der Komponente wird dann die Form definiert:
```html
<div className="row row-content">
    <div className="col-12 col-md-9">
        <Form onSubmit={this.handleSubmit}>
            <FormGroup row>
                <Label htmlFor="firstname" md={2}>First Name</Label>
                <Col md={10}>
                    <Input type="text" id="firstname" name="firstname" placeholder="First Name" value={this.state.firstname} onChange={this.handleInputChange} />
                </Col>
            </FormGroup>
            <!-- ... -->
        </Form>
    </div>
</div>
```
Um die Eingaben zu validieren muss der Zustand erweitert werden:
```JavaScript
class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // ...
            touched: {
                firstname: false,
                lastname: false,
                telnum: false,
                email: false
            }
            // ...
        }
        this.handleBlur = this.handleBlur.bind(this);
    }
```
Die Eingaben werden dann in der Funktion HandleBlur gemerkt:
```JavaScript
handleBlur = (field) => (evt) => {
    this.setState({
        // der ... Operator nimmt den alten State und ändert nur das rechts darin
        touched: {...this.state.touched, [field]: true}
    });
}

validate(firstname, lastname, telnum, email) {
    const errors = {
        firstname: '',
        lastname: '',
        telnum: '',
        email: ''
    };

    if (this.state.touched.firstname && firstname.length < 3) 
        errors.firstname = 'First Name should be >= 3 characters';
    // ...
}

render() {
    const errors = this.validate(this.state.firstname, this.state.lastname, this.state.telnum, this.state.email);
    // ...
        <FormGroup row>
            <Label htmlFor="firstname" md={2}>First Name</Label>
            <Col md={10}>
            <Input  type="text" 
                    id="firstname" 
                    name="firstname" 
                    placeholder="First Name" 
                    value={this.state.firstname} 
                    onChange={this.handleInputChange} 
                    onBlur={this.handleBlur('firstname')}
                    valid={errors.firstname === ''? 1 : undefined} 
                    invalid={errors.firstname !== '' ? 1 : undefined}/>
                    <FormFeedback>{errors.firstname}</FormFeedback>
            </Col>
        </FormGroup>
    // ...
}
```

## Uncontrolled Forms
Diese Forms speichern den Zustand nicht in der Komponente ab, sondern direkt im DOM des Browsers. Damit sind diese einfacher. Hier ein Beispiel für ein Login-Dialog:

```JavaScript
constructor(props){
        super(props);
        this.state={
        isNavOpen: false,
        isModalOpen: false
    };
    this.toggleNav = this.toggleNav.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
}

toggleModal() {
    this.setState({
        isModalOpen: !this.state.isModalOpen
    })
}

handleLogin(event) {
    console.log("handleLogin: Username: " + this.username.value + " Password:" +this.password.value + " Remember: " + this.remember.checked);
    this.toggleModal();
    event.preventDefault();
}
```
```html
<Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal} autoFocus={false}>
    <ModalHeader toggle={this.toggleModal}>Login</ModalHeader>
    <ModalBody>
        <Form onSubmit={this.handleLogin}>
            <FormGroup>
                <Label htmlFor="username">Username</Label>
                <Input type="text" id="username" name="username" getRef={(input) => this.username = input}/>
            </FormGroup>
            <FormGroup>
                <Label htmlFor="password">Password</Label>
                <Input type="password" id="password" name="password" getRef={(input) => this.password = input}/>
            </FormGroup>
            <FormGroup check>
                <Label check>
                    <Input type="checkbox" name="remember" getRef={(input) => this.remember = input} />
                    Remember me
                </Label>
            </FormGroup>
            <Button type="submit" value="submit" color="primary">Login</Button>
        </Form>
    </ModalBody>
</Modal>
```

## React Redux Forms
Für das Handling mit Redix Forms müssen einige Pakete installiert werden:
* react-redux
* react-redux-form
* redux

Redux nutzt einen zentralen Store zum Speichern der Daten. Diese werden dann nicht mehr in dem State der Komonente abgelegt, sondern halt dort. Dieser arbeitet nach dem Prinzip einer Nachrchtenqueue, die mit einem Reducer abgearbeitet wird. Der Zustand des Stores wird als immutable behandelt, also immer eine neue Kopie mit den Änderungen erzeugt, keine Änderungen daran vorgenommen. Das Prinzip kommt von Flux.

![Flux Architecture](./images/FluxArchitecture.png)

Mit der Software-Bibliothek Redux wird dieses Prinzip angewendet und stellt sich dann so dar:

![Redux Architecture](./images/ReduxArchitecture.png)

In den Code baut man es folgendermaßen ein.

```JavaScript
import {Provider} from 'react-redux';
import {ConfigureStore} from './redux/configureStore';

const store = ConfigureStore();

 class App extends Component {
   render() {
     //Einbetten des BrowserRouters in einen Provider. Der bekommt mit CreateStore einen Store
     return (
        <Provider store={store}>
            <BrowserRouter>
                <div> 
                    <Main />
                </div>
            </BrowserRouter>
        </Provider>
     );
   };
 }
```
In der Form kann dann der Mechanismus von Redux-Forms genutzt werden:
```JavaScript
import {Control, LocalForm, Errors} from 'react-redux-form';

class Contact extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(values) {
        console.log("Current State is: " + JSON.stringify(values));
        alert("Current State is: " + JSON.stringify(values));
    }
}
```
Im JSX:
```html
<LocalForm onSubmit={(values) => this.handleSubmit(values)}>
    <Row className="form-group">
        <Label htmlFor="firstname" md={2}>First Name</Label>
        <Col md={10}>
            <Control.text model=".firstname" 
                id="firstname" 
                name="firstname" 
                placeholder="First Name" 
                className="form-control"/>
        </Col>
```

Um Validierungen einzubauen wird noch etwas mehr benötigt
```JavaScript
const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => (val) && (val.length >= len);
const isNumber = (val) => !isNaN(Number(val));
const validEmail = (val) => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val);
```
```html
<LocalForm onSubmit={(values) => this.handleSubmit(values)}>
    <Row className="form-group">
        <Label htmlFor="firstname" md={2}>First Name</Label>
        <Col md={10}>
            <Control.text model=".firstname" 
                id="firstname" 
                name="firstname" 
                placeholder="First Name" 
                className="form-control"
                validators={{
                            required, 
                            minLength:minLength(3), 
                            maxLength:maxLength(15)
                        }}/>
            <Errors className="text-danger" model=".firstname" show="touched"
                messages={{
                    required: 'Required ',
                    minLength: 'Must be greater than 2 characters ',
                    maxLength: 'Must be 15 characters or less '
                }} />
        </Col>
    </Row>
</LocalForm>
```

## Split and Combine Reducers
Oft wird es notwendig den Store der Daten aufzutrennen, damit es übersichtlicher wird. Dafür gibt es CombinedReducer. Dieser sorgt dafür, dass es im Store ein Objekt gibt, aber es können verteilte Reducer genutzt werden. In unserem Beispiel:

```JavaScript
//configureStore.js
import {createStore,combineReducers} from 'redux';
import {Dishes} from './dishes.js';
import {Promotions} from './promotions.js';
import {Comments} from './comments.js';
import {Leaders} from './leaders.js';
 
export const ConfigureStore = () => {
     const store = createStore(
           combineReducers({
               dishes: Dishes,
               comments: Comments,
               promotions: Promotions,
               leaders: Leaders
           })
         );
 
     return store;
```
```JavaScript
//dishes.js
import {DISHES} from '../shared/dishes.js';

export const Dishes = (state = DISHES, action) => {
    switch(action.type) {
        default: 
            return state;
    }
} 
```

## Redux Actions
Diese Erweiterung nutzt nun den Redux-Mechanismus und definiert Events, die dann aus der UI gefeuert werden können. Dazu wird zunächst ein ActionCreator gebaut. Der erzeugt ein Objekt, welches im feld type den Typ der Action beinhaltet und in payload dann die dazu notwendigen Daten. Diese sind dann abhängig vom Typ des Events

```JavaScript
//ActionTypes.js
export const ADD_COMMENT = 'ADD_COMMENT';

//ActionCreators.js
import * as ActionTypes from './ActionTypes';

export const addComment = (dishId, rating, author, comment) => ({
    type: ActionTypes.ADD_COMMENT,
    payload: {
        dishId: dishId,
        rating: rating,
        author: author,
        comment: comment
    }
});

//MainComponent.js
import {addComment} from '../redux/ActionCreators';

const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        comments: state.comments
    }
}

const mapDispatchToProps = (dispatch) => ({
    addComment: (dishId, rating, author, comment) => dispatch(addComment(dishId,rating,author,comment))
});

// im JSX: 
<DishDetail dish={this.props.dishes.filter((dish) => dish.id === parseInt(match.params.dishId,10))[0]}
            comments={this.props.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId,10))}
            addComment={this.props.addComment}/>

//Die exportierte Komponete wird mit connect und withRouter immantelt
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Main));

```

## Redux Thunk
Benötigte Node-Module:
* redux-logger
* redux-thunk
Einbau in eine Komponente:

```JavaScript
//ActionTypes.js
export const ADD_COMMENT = 'ADD_COMMENT';
export const DISHES_LOADING = 'DISHES_LOADING';
export const DISHES_FAILED = 'DISHES_FAILED';
export const ADD_DISHES = 'ADD_DISHES';
```

```JavaScript
//Loading Component
import React from 'react';

export const Loading = () => {
    return(
        <div className="col-12">
            <span className="fa fa-spinner fa-pulse fa-3x fa-fw text-primary"></span>
            <p>Loading . . . </p>
        </div>
    );
};
```

```JavaScript
//DishDetailComponent.js
import {Loading} from './LoadingComponent';

 const DishDetail = (props) => {
    //während des Ladens
    if (props.isLoading) {
        return(
            <div className="container">
                <div className="row">
                    <Loading />
                </div>
            </div>
        );
    }
    // bei Auftreten eines Fehlers
    else if (props.errMess) {
        return(
            <div className="container">
                <div className="row">
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        );
    }
    // ... normaler Weg mit Daten
```

```JavaScript
//ActionCreators.js
import {DISHES} from '../shared/dishes.js';

//thunk: a function which returns a function. Diese erzeugt 2 Events
//       wird dann ausgeführt und erezugt weitere Events
//       damit kann dann erweiterte Steuerung erfolgen
export const fetchDishes = () => (dispatch) => {
    //feuert normales Event
    dispatch(dishesLoading(true));

    //feuert nach dem Timeout ein weiteres Event
    setTimeout(() => {
        dispatch(addDishes(DISHES));
    }, 2000);
}

export const dishesLoading = () => ({
    type: ActionTypes.DISHES_LOADING,
});

export const dishesFailed = (errmess) => ({
    type: ActionTypes.DISHES_FAILED,
    payload: errmess
});

export const addDishes = (dishes) => ({
    type: ActionTypes.ADD_DISHES,
    payload: dishes
});
```

```JavaScript
//MainComponent.js
import {addComment, fetchDishes} from '../redux/ActionCreators';

const mapDispatchToProps = (dispatch) => ({
addComment: (dishId, rating, author, comment) => dispatch(addComment(dishId,rating,author,comment)),
fetchDishes: () => {dispatch(fetchDishes())}
});

class Main extends Component {

    componentDidMount() {
        this.props.fetchDishes();
    }

     render() {
         const HomePage = () => {
             return(
                 <Home 
                     promotion={this.props.promotions.filter((promo) => promo.featured)[0]}
                     leader={this.props.leaders.filter((leader) => leader.featured)[0]}
                     dish={this.props.dishes.dishes.filter((dish) => dish.featured)[0]}
                     dishesLoading={this.props.dishes.isLoading}
                     dishesErrMess={this.props.dishes.errMess}
                 />
             );
         }

    const DishWithId = ({match}) => {
    return(
        <DishDetail dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId,10))[0]}
                    comments={this.props.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId,10))}
                    addComment={this.props.addComment}
                    isLoading={this.props.dishes.IsLoading}
                    ErrMess={this.props.dishes.errMess}/>
        );
    }
```

```JavaScript
//dishes.js
import * as ActionTypes from './ActionTypes';


export const Dishes = (state = {
                            isLoading: true,
                            errMess: null,
                            dishes: []
                        }, action) => {
    switch(action.type) {
        case ActionTypes.ADD_DISHES:
                return {...state, isLoading: false, errMess: null, dishes: action.payload};

        case ActionTypes.DISHES_LOADING:
                return {...state, isLoading: true, errMess: null, dishes: []};

        case ActionTypes.DISHES_FAILED:
                return {...state, isLoading: false, errMess: action.payload, dishes: []};

        default: 
            return state;
    }
} 
```

In der ConfigureStore werden die Klassen dann als MiddleWare eingetragen. Damit werden diese gerufen, wenn die Events verarbeitet werden. Der Thunk filtert die Funktionen auf Funktionen raus und bearbeitet diese. Events gehen durch. Der Logger schreibt die Events als Log-Ausschriften zum Debuggen raus.

```JavaScript
//configureStore.js
import {createStore,combineReducers, applyMiddleware} from 'redux';
import {Dishes} from './dishes.js';
import {Promotions} from './promotions.js';
import {Comments} from './comments.js';
import {Leaders} from './leaders.js';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

export const ConfigureStore = () => {
    const store = createStore(
           combineReducers({
               dishes: Dishes,
               comments: Comments,
               promotions: Promotions,
               leaders: Leaders
           }),
           applyMiddleware(thunk, logger)
        );

    return store;
}
```

## Promise

Das Prinzip von Promises ist hier recht ausführlich erklärt: 

[Promises Introduction](https://developers.google.com/web/fundamentals/primers/promises)

Promises sind eine Methode zur asynchronen Bearbeitung von Abläufen (z.B. Kommunikation mit einem Server), was es dem Anwender sehr einfach macht. Mit einem Promise wird also ein Prozess angestosen, der dann einen der 4 Zustände haben kann:

* fulfilled: Die Aktion ist erfolgreich beendet worden
* rejected: Die Aktion ist fehlerhaft beendet worden
* pending: Die Aktion läuft noch, es liegt noch kein Ergebnis vor
* settled: Wurde erfüllt oder rejected

Um einen Promise zu nutzen, werden zwei Funktion übergeben für Erfogl und Fehler

```js
var promise = new Promise(function(resolve, reject) {
  // do a thing, possibly async, then…

  if (/* everything turned out fine */) {
    resolve("Stuff worked!");
  }
  else {
    reject(Error("It broke"));
  }
});
```

Den Promise kann man dann in seinem Code einfach nutze:

```js
promise.then(function(result) {
  console.log(result); // "Stuff worked!"
}, function(err) {
  console.log(err); // Error: "It broke"
});
```

Die then-Klausel nimmt zwei Argumente. Einen für den Erfolgsfall und einen für den Fehlerfall. Beide sind optional. Die then()-Funktionen können hintereinander aufgereiht werden, wobei immer von oben nach untern abgearbeitet wird. Die Ergebnisse eines then() werden dann als Argument im nächsten übergeben. Auftretende Fehler können auch mit einem throw() als Exception geworfen werden, die dann mit der catch()-Funktion gefangen werden kann. Hier ein Beispiel:

```js
export const fetchDishes = () => (dispatch) => {
    dispatch(dishesLoading(true));

    return fetch(baseUrl + 'dishes')
        .then(response => {
                if (response.ok) {
                    return response;
                }
                else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errmess = new Error(error.message);
                throw errmess;
            })
        .then(response => response.json())
        .then(dishes => dispatch(addDishes(dishes)))
        .catch(error => dispatch(dishesFailed(error.message)));
}
```

In der Funktion wird ein fetch() gemacht, also Daten von einem REST-Server abgerufen. Das erhaltene Ergebnis (response) wird auf ok geprüft und im Fehlerfalle ein error geworfen. Sollte gar kein response zurück kommen, dann wird die failed-Funktion genutzt und ebenfalls ein error geworfen. Im Gutfalle wird der response in JSON geparst und das Event geworfen zum Aktualisieren der UI. Im Fehlerfalle wird mit dem catch() ein Fehler gefangen und ebenfalls ein Event gerufen, damit die UI dem User den Fehler anzeigen kann.

## React Animationen
Diese Komponenten helfen bei der Erstellung von Animationen im React. Es gibt sicher noch viele andere. Aber als erster Start gehen die:
* react-transition-group

### React-Transition-Group
Damit erstellst du einface CSS-Transitionen. Dafür müssen die CSS-Klassen definiert sein. Die dann genutzt werden. 

```js
class Main extends Component {
    return (
        <div>
            <Header />
            <TransitionGroup>
                <CSSTransition key={this.props.location.key} classNames="page" timeout={300}>
                    <Switch location={this.props.location}>
                        <Route path='/home' component={HomePage} />
                        <Route exact path='/aboutus' component={() => <About leaders={this.props.leaders} />} />} />
                        <Route exact path='/menu' component={() => <Menu dishes={this.props.dishes} />} />
                        <Route path='/menu/:dishId' component={DishWithId} />
                        <Route exact path='/contactus' component={() => <Contact resetFeedbackForm={this.props.resetFeedbackForm} postFeedback={this.props.postFeedback}/>} />
                        <Redirect to="/home" />
                    </Switch>
                </CSSTransition>
            </TransitionGroup>
            <Footer />
        </div>
    );
}
```
Dafür sind dann die CSS-Klassen notwendig:
```css
.page-enter {
  opacity: 0.01;
  transform: translateX(-100%);
}

.page-enter-active {
  opacity: 1;
  transform: translateX(0%);
  transition: all 300ms ease-in;
}

.page-exit {
  opacity: 1;
  transform: translateX(0%);
}

.page-exit-active {
  opacity: 0.01;
  transform: translateX(100%);
  transition: all 300ms ease-out;
}
```

### React-Animation-Components
Damit kannst du Animatonen in Komponenten nutzen. z.B.:

```js
import { Fade, Stagger } from 'react-animation-components';

function RenderLeaders({leaders}) {
    return (
        <Stagger in>
            {leaders.map((leader) => {
                return (
                    <Fade in>
                        <RenderLeader leader={leader} key={leader.id}/>
                    </Fade>);
            })}
        </Stagger>
    );  
}
```
Das führt zum langsamen einblenden der Einträge in leaders.
# Einrichten eines GIT Repositories

## Lokales GIT

### Init
```
git init
```
Einrichten eines neuen GIT Repositories (lokal) auf der Platte (.git)

### Status
```
git status
```
Damit kann man den Status abfragen. Im Ergebnis kommen dann die Files, die sich geändert haben oder neu sind. 

### Add Files
```
git add <file/folder>
git add .
```
Fügt die Dateien dem Repository hinzu, aber erst mal nur im lokalen Working Environment.

### Commit
```
git commit
```
Damit werden die Files in das Repository eingescheckt.

### Log Status
```
git log --oneline
```
Liefert einen schnellen Überblick über das Repository.
Beispiel:
```
20f1481 (HEAD -> master) React-Redux-Forms Revisited
8da77bc Redux Thunk introduced
5c87a6d Redux Actions
50abbf3 Split and Combine Reducers
da8c51a Assignment 3
440c891 React Redux Form Validation
9f056c6 React Redux Forms
4b1c5e4 Uncontrolled Forms
5107ab8 Controlled Forms
771e671 Controlled Forms
faaf408 Assignment 2
61d6959 Single Page Application Part 2
fadb056 Single Page Application Part 1
94b78bf React Router
66f9402 Header and Footer
c2ed905 Functional Components
201f111 Container and Presentational Components
fd89db2 Assignment 1 correction
a99a628 Assignment 1
9bdb9e5 React Components Part 2
1df32d1 React components Part 1
be28a79 Configure React
916790e Initial commit from Create React App
```

### Checkout

```
git checkout <commit> <file>
```
Dieses Kommando legt ein anderes File als das aktuelle in den Bearbeitungsbereich. Dieses kann dann bearbeitet und ggf. auch wieder eingecheckt werden.

### Reset
```
git reset <file>
git reset
```

Mit diesem Kommando wird das im Bearbeitungsbereich befindliche File rückgesetzt und durch das im Repository ersetzt. Ohne Angabe des Files wird alles rückgesetzt.
Mit der Option --hard wird die lokale Version der Dateien überschrieben mit der Version aus dem Repository.
```
git reset --hard
```

### Ignoieren von Dateien
Oft enthält das Projekt auch Dateien, die nicht im GIT-Repository benötigt werden. Insbesondere bei NPM sind dies die Dateien im /node_modules-Pfad. Auch logfiles o.ä. sollten nicht in das Repository integriert sein. Um das zu erreichen, wird eine Datei 

.gitignore

angelegt. Diese entält eine Auflistung von zu ignorierenden Pfaden/Files. Beispiel

```
# See https://help.github.com/articles/ignoring-files/ for more about ignoring files.

# vuepress
/docs/.vuepress/dist

# dependencies
/node_modules
/.pnp
.pnp.js

# testing
/coverage

# production
/build

# misc
.DS_Store
.env.local
.env.development.local
.env.test.local
.env.production.local

npm-debug.log*
yarn-debug.log*
yarn-error.log*
```

## Online GIT Repositories
Das lokale GIT kann auch einem Online-Repository abgelegt werden. Damit wird es dann auch für andere verfügbar und kann leicht veröffentlich werden. Zudem ist es da gesichert.
Mögliche Ablagen:
* [GitHub](https://github.com)
* [BitBucket](https://bitbucket.org)

### Verbinden des lokalen GIT mit dem Remote GIT:
```
git remote add origin <repository URL>
```
Damit wird das Remote-Repository in das lokale eingebunden. Die beiden können dann synchronisiert werden. 

### Update des Online Repositories
Um das remote respository zu aktualisieren. Wird dieses Kommando genutzt:
```
git push -u origin master
```
Fügt den Inhalt als neuen Checkin an den Branch Master an.

### Lokales Repository aus einem Remote erzeugen
Es ist einfach ein remote repository lokal zu erzeugen, um damit dann arbeiten zu können:
```
git clone <repository URL>
```


# Entwicklertools
Hier werde ich ein paar interessante Tools auflisten, die man beim Entwickeln ganz gut gebrauchen kann. Ist sicher nicht vollständig, aber hilft vielleicht zum Starten.

## JSON-Server
Damit wird ein einfacher REST-Server realisiert. Er kann eine Json-Datei überwachen und bildet die Arrays da drinnen als Services im REST ab. Man kann so die Daten lesen und auch schreiben.

* [json-server](https://github.com/typicode/json-server)
* [Creating Demo APIs with json-server](https://egghead.io/lessons/javascript-creating-demo-apis-with-json-server)
* [JSON](https://www.json.org/json-en.html)

## Expo als Entwicklungsumgebung
Am einfachsten scheint die Entwicklung mit Expo zu funktionieren. Dafür muss ein Account auf der Seite [https://expo.io](https://expo.io) angelegt werden. Über die loakle CLI kann das erstellte Projekt dann hochgeladen werden. Damit wird bereits die Nutzung auf dem lokalen Gerät möglich, auch ohne Store-Account. Man hat dort die Möglichkeit den Link auf das Projekt zu verteilen. Mit Hilfe der auf dem Gerät installierten expo-app kann jeder dann die App nutzen.

Zusätzlich bietet expo die Möglichkeit die App zu bundlen und an den AppStore zu übergeben. Dafür muss dann im iTunesConnect eine App angelegt werden und diese mit dem Projekt verknüpft sein. Gleiches gilt für die Android App. Dann kann mit einem einfach command-prompt-Befehl der Prozess angestoßen werden. Die App wird dann gebaut und in den Appstore geladen. Damit kann man dann diese freigeben.


## Lokale Entwicklung auf iOS mit XCode
Damit das geht, muss man aus der EXPO-Umgebung exejecten. Das wrid mit einem Befehl expo eject gemacht. Das führt zu Erstellen von zwei Directories mit den nativen Projekten für Android und iOS. Für iOS wird dann ein MAC uns XCode benötigt. Zuvor muss noch CocoaPods zum Managen der Dependencies installiert sein:

```
sudo gem install cocoapods
```
Dann in den Folder iOS wechseln und die Abhängigkeiten installieren:
```
pod install
```
Danach kann das Projekt dann mit XCode geöffnet und gebaut werden.

## Homebrew
Dies ist ein Paketmanager für iOS, der es einfacher macht Apps und andere Software zu installieren. Installieren nach Angaben auf der Homepage [Homebrew](https://brew.sh/index_de), aktuell:
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```
## Mongo DB
Hierbei handelt es sich um eine Dokumentenbasierte Datenbank. Zu finden unter [MongoDB](https://mongodb.com). Unter MacOS am einfachsten zu installieren mit Homebrew. Dazu erst mal das Paket beschaffen: 
```
brew tap mongodb/brew
````
dann installieren mit:
```
brew install mongodb-community@4.2
```
Damit ist die Software erst mal drauf. Zum Starten ein Verzeichnis anlegen. Hier mal als Beispiel unter /Users/tommy/Development/MongoDB. Dort drin dann einen Folder für die Datenbank anlegen. Z.B. /data. Dann Server starten mit:
```
mongod --dbpath=data --bind_ip 127.0.0.1
```
Das startet den Dienst auf der IP 127.0.0.1 und speichert eine neue Datenbank in den data-Pfad.
Zum Anlegen einer neuen Datenbank im Server einfach in diesen wechseln. Sollte der noch nicht existieren, dann wird er angelegt. Um das zu initiieren, kann der mongo-CLI genutzt werden.
```
mongo

//am prompt dann eingeben
// erzeugt eine neue Datenbank conFusion
use conFusion

//neue Daten einfügen
db.dishes.insert({ name: "Uthappizza", description: "Test" });

//Abfragen der Daten
db.dishes.find().pretty();
```



